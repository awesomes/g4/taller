package com.group4;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    static int counter = 0;

    public static void main(String[] args) {

        boolean answer;
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int choice;
        do {
            System.out.println("Bienvenido.");
            System.out.println("1 -> Ejercicios Matematicos");
            System.out.println("2 -> Parqueadero");
            System.out.println("3 -> Loteria");
            System.out.println("4 -> Encender Televisor");
            System.out.println("5 -> Salir");
            System.out.print("Escoja la opción deseada: ");
        choice = scanner.nextInt();
        switch (choice) {
            case 1:
                Menu1 menu1 = new Menu1();
                menu1.Menu();
                break;
            case 2:
                Parqueadero1 parqueadero = new Parqueadero1();
                parqueadero.Parqueadero1ay();
                break;
            case 3:
                Loteria lot = new Loteria();
                lot.Loterias();
                break;
            case 4:
                Televisor TV = new Televisor();
                TV.TVMenu();
                break;
            case 5:
                System.out.println("Gracias por usar nuestros servicios :)");
                break;
            default:
                System.out.println("Seleccion invalida");
        }

        }while(choice != 5);
    }
    }

