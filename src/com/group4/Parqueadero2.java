package com.group4;

import java.util.ArrayList;
import java.util.Scanner;


public class Parqueadero2 {
    private String[] arregloCupos;
    private String carro;
    private String bici;
    private String moto;

    public int getCantidadCarros() {
        return cantidadCarros;
    }

    public void setCantidadCarros(int cantidadCarros) {
        this.cantidadCarros = cantidadCarros;
    }

    private int cantidadCarros;
    private int cantidadBici;
    private int cantidadMoto;

    public String[] getArregloCupos() {
        return arregloCupos;
    }

    public void setArregloCupos(String[] arregloCupos) {
        this.arregloCupos = arregloCupos;
    }

    public String getCarro() {
        return carro;
    }

    public void setCarro(String carro) {
        this.carro = carro;
    }

    public String getBici() {
        return bici;
    }

    public void setBici(String bici) {
        this.bici = bici;
    }

    public String getMoto() {
        return moto;
    }

    public void setMoto(String moto) {
        this.moto = moto;
    }

    public int getCantidadBici() {
        return cantidadBici;
    }

    public void setCantidadBici(int cantidadBici) {
        this.cantidadBici = cantidadBici;
    }

    public int getCantidadMoto() {
        return cantidadMoto;
    }

    public void setCantidadMoto(int cantidadMoto) {
        this.cantidadMoto = cantidadMoto;
    }

    public Parqueadero2(){ //Metodo constructor
        arregloCupos = new String[5];

    }

    //Metodo para comprobar el parqueadero
    public boolean comprobarCupoParqueadero(){
        if (cantidadBici>=3)
        {
            System.out.println("no hay mas cupos para bicicletas,solo pueden ingresar carros");
            cantidadBici = 0;
            cantidadMoto = 0;
            return false;
        }
        else if (cantidadMoto>=2)
        {
            System.out.println("No hay cupos para motos, solo pueden ingresar carros");
            cantidadBici = 0;
            cantidadMoto = 0;
            return false;
        }
        else if (cantidadCarros>=5)
        {
            System.out.println("No hay cupos disponibles en el estacionamiento");
        }
        return true;
    }
}
