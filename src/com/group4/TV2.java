package com.group4;

public class TV2 {
    public static int TVCanal(){
        System.out.println("¿Que canal desea colocar? 1-99");
        int canalalt = Televisor.scanner.nextInt();
        if (canalalt <= 0 || canalalt >= 100){
            System.out.println("Escoja un canal entre 1 a 99");
        }
        else {
            Televisor.canal = canalalt;
        }
        return Televisor.canal;
    }
    public static void TVCanalUp(){
        Televisor.canal = Televisor.canal + 1;
        if (Televisor.canal > 100){
            Televisor.canal = Televisor.canal - 1;
        }
    }
    public static void TVCanalDown(){
        Televisor.canal = Televisor.canal - 1;
        if (Televisor.canal < 1){
            Televisor.canal = Televisor.canal + 1;
        }
    }

    public static void TVVolumenMute(){
        if (Televisor.mute == false){
            Televisor.mute = true;
        }
        else {
            Televisor.mute = false;
        }
    }
    public static void TVVolumenUp(){
        if (Televisor.mute == true){
            Televisor.mute = false;
        }
        Televisor.volumen = Televisor.volumen + 1;
        if (Televisor.volumen > 100){
            Televisor.volumen = Televisor.volumen - 1;
        }
    }
    public static void TVVolumenDown(){
        if (Televisor.mute = true){
            Televisor.mute = false;
        }
        Televisor.volumen = Televisor.volumen - 1;
        if (Televisor.volumen < 0){
            Televisor.volumen = Televisor.volumen + 1;
        }
    }

    public static void TVNConexion(){
        int seleccion,misinput=1;
        do {
            System.out.println("¿Que puerto desea conectar?");
            System.out.println("1 -> HDMI");
            System.out.println("2 -> VGA");
            System.out.println("3 -> DisplayPort");
            System.out.println("4 -> Volver a TV");

            System.out.print("Inserte su seleccion:");
            seleccion = Televisor.scanner.nextInt();

            switch (seleccion){
                case 1: HDMI(); break;
                case 2: VGA(); break;
                case 3: DisplayPort(); break;
                case 4: misinput=0; break;
                default: System.out.println("Inserte seleccion valida.");
            }
        }while (misinput!=0);
    }
    public static void HDMI(){
        int seleccion, misinput =1;
        do {
            System.out.println("HDMI conectado");
            if (Televisor.mute == true){
                System.out.println("Volumen: Mute");
            }
            else {
                System.out.println("Volumen: " + Televisor.volumen);
            }
            System.out.println("1 -> Conectar otro puerto");
            System.out.println("2 -> Mute");
            System.out.println("3 -> Subir volumen");
            System.out.println("4 -> Bajar volumen");

            System.out.print("Inserte su seleccion:");
            seleccion = Televisor.scanner.nextInt();

            switch (seleccion){
                case 1: misinput=0; break;
                case 2: TVVolumenMute(); break;
                case 3: TVVolumenUp(); break;
                case 4: TVVolumenDown(); break;
                default: System.out.println("Inserte seleccion valida.");
            }
        }while (misinput!=0);
    }
    public static void VGA(){
        int seleccion, misinput =1;
        do {
            System.out.println("VGA conectado");
            if (Televisor.mute == true){
                System.out.println("Volumen: Mute");
            }
            else {
                System.out.println("Volumen: " + Televisor.volumen);
            }
            System.out.println("1 -> Conectar otro puerto");
            System.out.println("2 -> Mute");
            System.out.println("3 -> Subir volumen");
            System.out.println("4 -> Bajar volumen");

            System.out.print("Inserte su seleccion:");
            seleccion = Televisor.scanner.nextInt();

            switch (seleccion){
                case 1: misinput=0; break;
                case 2: TVVolumenMute(); break;
                case 3: TVVolumenUp(); break;
                case 4: TVVolumenDown(); break;
                default: System.out.println("Inserte seleccion valida.");
            }
        }while (misinput!=0);
    }
    public static void DisplayPort(){
        int seleccion, misinput =1;
        do {
            System.out.println("DisplayPort conectado");
            if (Televisor.mute == true){
                System.out.println("Volumen: Mute");
            }
            else {
                System.out.println("Volumen: " + Televisor.volumen);
            }
            System.out.println("1 -> Conectar otro puerto");
            System.out.println("2 -> Mute");
            System.out.println("3 -> Subir volumen");
            System.out.println("4 -> Bajar volumen");

            System.out.print("Inserte su seleccion:");
            seleccion = Televisor.scanner.nextInt();

            switch (seleccion){
                case 1: misinput=0; break;
                case 2: TVVolumenMute(); break;
                case 3: TVVolumenUp(); break;
                case 4: TVVolumenDown(); break;
                default: System.out.println("Inserte seleccion valida.");
            }
        }while (misinput!=0);
    }










    //----------------------------------------CODIGO NO USADO-----------------------------------------------------------
    public static void TVInfo(){
        System.out.print("Los puertos conectados son:" );
        TV2.TVConectados();
        System.out.println();
        System.out.println("El canal es: " + Televisor.canal + " y el volumen es " + Televisor.volumen);
    }
    public static void TVConexiones(){
        int con = 0;
        for (int i = 0; i < Televisor.nconexion.length; i++) {
            System.out.println("¿Desea conectar el puerto " + Televisor.nconexion[i] + "? Si=1 No=2");
            con = Televisor.scanner.nextInt();
            if (con == 1) {
                Televisor.conexiones[i] = true;
            } else if (con == 2) {
                Televisor.conexiones[i] = false;
            } else {
                System.out.println("Use solo 1 o 2, se tomará como no conectado.");
                Televisor.conexiones[i] = false;
            }
        }
        TV2.TVConectados();
        System.out.println();
    }
    public static void TVConectados(){
        for (int j = 0; j < Televisor.nconexion.length; j++) {
            if (Televisor.conexiones[j] == true) {
                System.out.print(Televisor.nconexion[j] + " ");
            }
        }
    }
    public static int TVVolumen(){
        System.out.println("Ingrese el volumen de 0 a 100");
        Televisor.volumen = Televisor.scanner.nextInt();
        return Televisor.volumen;
    }
}
