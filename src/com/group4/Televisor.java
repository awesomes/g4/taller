package com.group4;

import java.util.Scanner;

public class Televisor {
    static Scanner scanner = new Scanner(System.in);
    int seleccion = 0;
    static int canal = 1, volumen = 1;
    static boolean[] conexiones = new boolean[3];
    static String[] nconexion = {"HDMI", "VGA", "Display"};
    static boolean mute;

    public static void TVMenu(){

        int seleccion=0,encendido=1;
        mute = false;
        do {
            Scanner scanner = new Scanner(System.in);
            if (mute == true){
                System.out.println("Canal: " + canal +" Volumen: Mute");
            }
            else {
                System.out.println("Canal: " + canal +" Volumen: " + volumen);
            }

            System.out.println("1 -> Apagar TV");
            System.out.println("2 -> Subir canal");
            System.out.println("3 -> Bajar canal");
            System.out.println("4 -> Poner canal por numero");
            System.out.println("5 -> Mute");
            System.out.println("6 -> Subir volumen");
            System.out.println("7 -> Bajar volumen");
            System.out.println("8 -> Cambiar puerto conectado");

            System.out.print("Inserte su seleccion:");
            seleccion = scanner.nextInt();

            switch (seleccion) {
                case 1:
                    encendido = 0;
                    break;
                case 2:
                    TV2.TVCanalUp();
                    break;
                case 3:
                    TV2.TVCanalDown();
                    break;
                case 4:
                    TV2.TVCanal();
                    break;
                case 5:
                    TV2.TVVolumenMute();
                    break;
                case 6:
                    TV2.TVVolumenUp();
                    break;
                case 7:
                    TV2.TVVolumenDown();
                    break;
                case 8:
                    TV2.TVNConexion();
                    break;
                default:
                    System.out.println("Inserte seleccion valida.");
            }
        }while (encendido != 0);
    }
}
