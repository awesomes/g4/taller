package com.group4;

import java.util.Locale;
import java.util.Scanner;

public class Menu1 {
    public static void Menu(){
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int counter1 = 0, numA,numB,numC,numD,numE, choice = 0;
        byte numF;
        double numG;
        int[] numbers;
        String ph1, ph2;

        while (counter1 == 0) {
            System.out.println("Bienvenido a Agilidad, Conversiones y Geomnetría" + "\n" + "Escoja la sección a realizar" + "\n"
                    + "Agilidad : 1" + "\n" + "Conversion: 2" + "\n" + "Geometria: 3" + "\n" + "Salir: 4");
            System.out.println("Escoja su Opcion: ");
            choice = scanner.nextInt();
            if (choice != 0) {
                counter1 = 1;
            }
            switch (choice) {
                case 1:
                    System.out.println("Bienvenido a Agilidad" + "\n" + "Escoja su ejercicio a realizar: ");
                    System.out.println("Mas Grande que: Oprima 1" + "\n" + "Ordenar Numeros: Oprima 2" + "\n" + "Mas pequeño que : Oprima 3"
                            + "\n" + "Numero Palindromo: Oprima 4" + "\n" + "Palabra Palindroma: Oprima 5" + "\n" + "Factorial: Oprima 6" + "\n" +
                            "Numero Impar: Oprima 7" + "\n" + "Numero Primo: Oprima 8"+ "\n" + "Numero Par: Oprima 9" + "\n" + "Numero Perfecto: Oprima 10"
                            + "\n" + "Fibonacci: Oprima 11" + "\n" + "Veces que son divididas por 3: Oprima 12" + "\n" + "FizzBuzz: Oprima 13");
                    System.out.print("Ingrese su Opción :");
                    choice = scanner.nextInt();
                    if (choice != 0){
                        Main.counter = 1;
                    }
                    switch (choice) {
                        case 1:
                            System.out.println("Ingrese Numero 1");
                            ph1 = scanner.next();
                            System.out.println("Ingrese Numero 2");
                            ph2 = scanner.next();

                            System.out.println("El resultado es: " + Agility.biggerThan(ph1, ph2));
                            break;
                        case 2:
                            System.out.println("Ingrese 5 Numeros");
                            System.out.println("Ingrese Numero 1");
                            numA = scanner.nextInt();
                            System.out.println("Ingrese Numero 2");
                            numB = scanner.nextInt();
                            System.out.println("Ingrese Numero 3");
                            numC = scanner.nextInt();
                            System.out.println("Ingrese Numero 4");
                            numD = scanner.nextInt();
                            System.out.println("Ingrese Numero 5");
                            numE = scanner.nextInt();

                            Agility.order(numA, numB, numC, numD, numE);
                            break;
                        case 3:
                            System.out.println("Ingrese 5 Numeros");
                            System.out.println("Ingrese Numero 1");
                            numA = scanner.nextInt();
                            System.out.println("Ingrese Numero 2");
                            numB = scanner.nextInt();
                            System.out.println("Ingrese Numero 3");
                            numC = scanner.nextInt();
                            System.out.println("Ingrese Numero 4");
                            numD = scanner.nextInt();
                            System.out.println("Ingrese Numero 5");
                            numE = scanner.nextInt();

                            System.out.println("El numero menor es: " + Agility.smallerThan(numA, numB, numC, numD, numE));
                            break;
                        case 4:
                            System.out.println("Ingrese su numero a verificar");
                            numA = scanner.nextInt();

                            System.out.println("El resultado es: " + Agility.palindromeNumber(numA));
                            break;
                        case 5:
                            System.out.println("Ingrese su palabra a verificar");
                            ph1 = scanner.next();

                            System.out.print("El resultado es: " + Agility.palindromeWord(ph1));
                            break;
                        case 6:
                            System.out.println("Ingrese su numero a hacer proceso Factorial");
                            numA = scanner.nextInt();

                            System.out.println(Agility.factorial(numA));
                            break;
                        case 7:
                            System.out.println("Ingrese Numero");
                            numF = scanner.nextByte();
                            System.out.print("El resultado es: " + Agility.isOdd(numF));
                            break;
                        case 8:
                            System.out.println("Ingrese numero a Valorar");
                            numA = scanner.nextInt();
                            System.out.println("El resultado es: " + Agility.isPrimeNumber(numA));
                            break;
                        case 9:
                            System.out.println("Ingrese numero a Valorar");
                            numF = scanner.nextByte();
                            System.out.println("El resultado es: " + Agility.isEven(numF));
                            break;
                        case 10:
                            System.out.println("Ingrese numero a Valorar");
                            numA = scanner.nextInt();
                            System.out.println("El resultado es: " + Agility.isPerfectNumber(numA));
                            break;
                        case 11:
                            System.out.println("Ingrese numero a realizar serie Fibonacci");
                            numA = scanner.nextInt();
                            System.out.println(Agility.fibonacci(numA));
                            break;
                        case 12:
                            System.out.println("Ingrese numero a dividir por tres");
                            numA = scanner.nextInt();
                            System.out.println("El resultado es: " + Agility.timesDividedByThree(numA));
                            break;
                        case 13:
                            System.out.println("Ingrese numero a jugar Fizzbuzz");
                            numA = scanner.nextInt();
                            System.out.println("El resultado es: " + Agility.fizzBuzz(numA));
                            break;
                        default:
                            System.out.println("Ingrese Numero Valido");
                            Main.counter = 0;


                    }
                    break;

                case 2:
                    System.out.println("Bienvenido a Conversion" + "\n" + "Escoja su ejercicio a realizar: ");
                    System.out.println("Kilometros a Metros 1" + "\n" + "Kilometros a Centimetros: Oprima 2" + "\n" + "Milimietros a metros : Oprima 3"
                            + "\n" + "Millas a Pies: Oprima 4" + "\n" + "Yardas a Pulgadas: Oprima 5" + "\n" + "Pulgadas a Millas: Oprima 6" + "\n" +
                            "Pies a Yardas: Oprima 7" + "\n" + "Kilometros a Pulgadas: Oprima 8"+ "\n" + "Milimetros a Pies: Oprima 9" + "\n" + "Yardas a Centimetros: Oprima 10");

                    System.out.print("Ingrese su Opción :");
                    choice = scanner.nextInt();
                    if (choice != 0){
                        Main.counter = 1;
                    }
                    switch (choice) {
                        case 1:
                            System.out.println("Ingrese Numero de Kilometros a convertir");
                            numG = scanner.nextDouble();

                            System.out.println("El resultado es: " + Convertion.kmTom(numG) +" metros");
                            break;
                        case 2:
                            System.out.println("Ingrese Numero de Kilometros a convertir");
                            numG = scanner.nextDouble();

                            System.out.println("El resultado es: " + Convertion.kmTocm(numG) +" centimetros");
                            break;
                        case 3:
                            System.out.println("Ingrese Numero de milimetros a convertir");
                            numA = scanner.nextInt();

                            System.out.println("El resultado es: " + Convertion.mmTom(numA) +" metros");
                            break;
                        case 4:
                            System.out.println("Ingrese Numero de millas a convertir");
                            numG = scanner.nextDouble();

                            System.out.println("El resultado es: " + Convertion.milesToFoot(numG) +" pies");
                            break;
                        case 5:
                            System.out.println("Ingrese Numero de Yardas a convertir");
                            numA = scanner.nextInt();

                            System.out.println("El resultado es: " + Convertion.yardToInch(numA) +" Pulgadas");
                            break;
                        case 6:
                            System.out.println("Ingrese Numero de Pulgadas a convertir");
                            numG = scanner.nextDouble();

                            System.out.println("El resultado es: " + Convertion.inchToMiles(numG) +" Millas");
                            break;
                        case 7:
                            System.out.println("Ingrese Numero de Pies a convertir");
                            numA = scanner.nextInt();

                            System.out.println("El resultado es: " + Convertion.footToYard(numA) +" Yardas");
                            break;
                        case 8:
                            System.out.println("Ingrese Numero de Kilometros a convertir");
                            numA =  scanner.nextInt();

                            System.out.println("El resultado es: " + Convertion.kmToInch(numA) +" Pulgadas");
                        case 9:
                            System.out.println("Ingrese Numero de mmilimetros a convertir");
                            numA = scanner.nextInt();

                            System.out.println("El resultado es: " + Convertion.mmToFoot(numA) +" Pies");
                            break;
                        case 10:
                            System.out.println("Ingrese Numero de yardas a convertir");
                            numA = scanner.nextInt();

                            System.out.println("El resultado es: " + Convertion.yardToCm(numA) +" centimetros");
                            break;
                        default:
                            System.out.println("Ingrese Numero Valido");
                            Main.counter = 0;

                    }

                    break;
                case 3:
                    System.out.println("Bienvenido a Geometria" + "\n" + "Escoja su ejercicio a realizar: ");
                    System.out.println("Area Cuadrado: Oprima 1" + "\n" + "Area del Rectangulo: Oprima 2" + "\n" + "Area del Circulo: Oprima 3"
                            + "\n" + "Perimetro Cuadrado: Oprima 4" + "\n" + "Perimetro de un Circulo: Oprima 5" + "\n" + "Volumen de una Esfera: Oprima 6" + "\n" +
                            "Area del Pentagono: Oprima 7" + "\n" + "Hipotenusa: Oprima 8");

                    System.out.print("Ingrese su Opción :");
                    choice = scanner.nextInt();
                    if (choice != 0){
                        Main.counter = 1;
                    }
                    switch (choice) {
                        case 1:
                            System.out.println("Ingrese Numero del lado del cuadrado");
                            numA = scanner.nextInt();

                            System.out.println("El resultado es: " + Geometric.squareArea(numA));
                            break;
                        case 2:
                            System.out.println("Ingrese Numero del lado 1 del rectangulo");
                            numA = scanner.nextInt();
                            System.out.println("Ingrese Numero del lado  2 del rectangulo");
                            numB = scanner.nextInt();

                            System.out.println("El resultado es: " + Geometric.squareArea(numA,numB));
                            break;
                        case 3:
                            System.out.println("Ingrese Numero del radio del circulo");
                            numG = scanner.nextDouble();

                            System.out.println("El resultado es: " + Geometric.circleArea(numG));
                            break;
                        case 4:
                            System.out.println("Ingrese Numero del lado del cuadrado");
                            numG = scanner.nextDouble();

                            System.out.println("El resultado es: " + Geometric.squarePerimeter(numG));
                            break;
                        case 5:
                            System.out.println("Ingrese Numero del diametro del circulo");
                            numA = scanner.nextInt();

                            System.out.println("El resultado es: " + Geometric.circleArea(numA));
                            break;
                        case 6:
                            System.out.println("Ingrese Numero del radio de la esfera");
                            numG = scanner.nextDouble();

                            System.out.println("El resultado es: " + Geometric.sphereVolume(numG));
                            break;
                        case 7:
                            System.out.println("Ingrese Numero del lado del pentagono");
                            numA = scanner.nextInt();

                            System.out.println("El resultado es: " + Geometric.pentagonArea(numA));
                            break;
                        case 8:
                            System.out.println("Ingrese Numero del cateto 1 del ");
                            numA = scanner.nextInt();
                            System.out.println("Ingrese Numero del cateto 2 del rectangulo");
                            numB = scanner.nextInt();

                            System.out.println("El resultado es: " + Geometric.calculateHypotenuse(numA,numB));
                            break;
                        default:
                            System.out.println("Ingrese Numero Valido");
                            Main.counter = 0;
                    }

                    break;
                case 4:
                    System.out.println("Gracias por usar nuestros servicios :)");
                    break;
                default:
                    System.out.println("Ingrese Numero Valido");
                    Main.counter = 0;
            }
    }


    }
}
